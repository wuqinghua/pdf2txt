#ifndef _UTIL_H_
#define _UTIL_H_
#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <stdbool.h>

struct file_dscr {
	void *cont;
	size_t size;
};

struct str_node {
	char *str;
	struct str_node *next;
};

int seek_read(FILE *p, uint32_t offset, char *ptr, uint32_t size);
int read_in_mem(struct file_dscr *fd, uint32_t offset, void *ptr, uint32_t size);
int dump_to_file(struct str_node *list, const char *outputfile);
void free_str_list(struct str_node *list);
int map_file_to_mem(char *filename, struct file_dscr *fd);

#endif /* _UTIL_H_ */
