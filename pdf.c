#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#define __USE_GNU
#include <string.h>
#include <locale.h>
#include <wchar.h>
#include <errno.h>
#include <zlib.h>
#include <ctype.h>
#include <dirent.h>

#include <sys/time.h>

#include "util.h"
#include "pdf.h"

static struct cmap *cmap_list = NULL;

int pdf_global_resource_init()
{
	char *cmap_path = PDF_RESOURCE_PATH "/cmap";

	/* read each file in this directory and parse it */
	struct dirent *file_ptr;   
	DIR *dir;
	int ret = 0;

	dir = opendir(cmap_path);
	if (!dir) {
		fprintf(stderr, "error: can't read cmap directory\n");
		return -1;
	}

	struct cmap *cmap_tail = NULL,
				*cmap_ptr = NULL;
	while((file_ptr = readdir(dir)))
	{
		/* jump over '.' and '..' */
		if(!strcmp(file_ptr->d_name, ".") || !strcmp(file_ptr->d_name, ".."))
			continue;

		cmap_ptr = (struct cmap*)malloc(sizeof(struct cmap));
		if (!cmap_ptr) {
			fprintf(stderr, "error: malloc memory failed\n");
			ret = -1;
			break;
		}

		memset(cmap_ptr, 0, sizeof(struct cmap));

		ret = read_cmap_file(cmap_path, file_ptr->d_name, cmap_ptr);
		if (ret == -1) {
			fprintf(stderr, "error: can't parse cmap file\n");
			free(cmap_ptr);
			continue;
		}

		if (cmap_list) {
			cmap_tail->next = cmap_ptr;
			cmap_tail = cmap_tail->next;
		}
		else {
			cmap_list = cmap_tail = cmap_ptr;
		}
	}

	closedir(dir);

	return ret;
}

int pdf_global_resource_release()
{
	struct cmap *ptr = NULL;
	while ((ptr = cmap_list)) {
		cmap_list = cmap_list->next;
		free(ptr);
	}

	cmap_list = NULL;

	return 0;
}

inline int is_name_obj(char *content)
{
	return (content && *content == '/');
}

inline int is_reference(char *content)
{
	char r;
	int obj_num;
	short gen;
	int ret = 0;

	if (!content)
		return 0;

	ret = sscanf(content, "%d %hd %c", &obj_num, &gen, &r);

	return (ret == 3);
}

inline void skip_blank_in_string(char **bufp)
{
	while ((**bufp == '\n' || **bufp == '\t'|| **bufp == ' ' ||
			**bufp == '\r' || **bufp == '\014') && **bufp != '\0') 
		(*bufp)++;
}

inline void skip_words(char **bufp, int num)
{
	int i = 0;
	while (i++ < num) {
		while (!isspace((int)(*((*bufp)++))));
		while (isspace((int)(*((*bufp)++))));
	}
}

inline void skip_line(char **bufp)
{
	if (**bufp == '\0') {
		(*bufp)++;
		return ;
	}

	while (**bufp != '\n' && **bufp != '\r' && **bufp != '\0')
		(*bufp)++;
	
	if (**bufp == '\0')
		return ;

	(*bufp)++;	/* skip '\n' or '\r' */

	if ((**bufp == '\n' || **bufp == '\r'))
		(*bufp)++;
}

char parse_escape_char(const char **line)
{
	char ret = 0;
	if (isdigit(**line)) {
		int i = 0;
		while (i < 3 && isdigit(**line)) {
			ret *= 8;
			ret += (**line) - '0';
			(*line)++;
		}
	}
	else {
		char ch = **line;
		if (ch == 'n') {
			ret = '\n';
		}
		else if (ch == 'r') {
			ret = '\r';
		}
		else if (ch == 't') {
			ret = '\t';
		}
		else if (ch == 'b') {
			ret = '\b';
		}
		else if (ch == 'f') {
			ret = '\f';
		}
		else if (ch == '(') {
			ret = '(';
		}
		else if (ch == ')') {
			ret = ')';
		}
		else if (ch == '\\') {
			ret = '\\';
		}
		else {
			return 0;
		}

		(*line)++;
	}

	return ret;
}

int convert_to_cmap_file_name(const char *from, char *to)
{
	if (!strncmp(from, "B5pc", 4)) {
		strcpy(to, "B5pc-UCS2C");
		return 0;
	}
	if (!strncmp(from, "ETen-B5", 7)) {
		strcpy(to, "ETen-B5-UCS2");
		return 0;
	}
	if (!strncmp(from, "GBK-EUC", 7)) {
		strcpy(to, "GBK-EUC-UCS2");
		return 0;
	}
	if (!strncmp(from, "GBpc-EUC", 8)) {
		strcpy(to, "GBpc-EUC-UCS2C");
		return 0;
	}
	/* else */
	to[0] = '\0';
	return -1;
}

int lookup_word_in_global_cmap(const char *font_name, uint16_t code)
{
	char file_name[64];
	int ret = 0;
	ret = convert_to_cmap_file_name(font_name, file_name);
	if (ret == -1) {
		//	fprintf(stderr, "error: can't find the exact cmap file\n");
		return -1;
	}

	struct cmap *cmap_ptr = cmap_list;
	while ((cmap_ptr)) {
		if (strcmp(cmap_ptr->cmap_file_name, file_name)) {
			break;
		}
		cmap_ptr = cmap_ptr->next;
	}
	if (!cmap_ptr) {
		//	fprintf(stderr, "error: can't find char encoding in cmap file\n");
		return -1;
	}

	uint16_t res = cmap_ptr->array[code];
	
	if (res == UINT16_MAX)
		return -1;
	else 
		return (int)(res);
}

int read_range_map_in_cmap_file(const char *stream, int count, uint16_t *array, size_t size)
{
	int ret = 0;
	int i = 0;
	uint16_t begin, end, to;
	char *cont = (char *)stream;
	char *cont_end = cont + strlen(cont);

	while (i++ < count && cont < cont_end) {
		ret = sscanf(stream, "<%hx> <%hx> <%hx>", &begin, &end, &to);
		if (ret != 3) {
		//	fprintf(stderr, "error: read range map failed\n");
			return -1;
		}

		while (begin <= end)
			array[begin++] = (to++);
		
		skip_line(&cont);
		skip_blank_in_string(&cont);
	}

	return 0;
}

int parse_cmap_file(struct file_dscr *fd, uint16_t *array, size_t size)
{
	int ret = 0;

	char *line_begin = (char *)(fd->cont);
	char *line_end = (char *)(fd->cont);
	char *cont_end = (char *)(fd->cont) + fd->size;
	skip_line(&line_end);

	memset(array, (int)(-1), size * sizeof(uint16_t));
	
	int count = 0;
	/* get the range map*/
	while (line_end < cont_end) {
		while (line_end < cont_end  && 
				!memmem((const void *)(line_begin), (line_end - line_begin),
					(const void *)("beginbfrange"), 12)) {
			line_begin = line_end;
			skip_line(&line_end);
		}
		
		if (!(line_end < cont_end)) {
			break;
		}

		/* next range map */
		ret = sscanf(line_begin, "%d", &count);
		if (ret != 1) {
		//	fprintf(stderr, "error: read number of range map failed\n");
			return -1;
		}

		skip_line(&line_begin);
		skip_blank_in_string(&line_begin);

		/* read the map */
		ret = read_range_map_in_cmap_file(line_begin, count, array, size);
		if (ret == -1) {
		//	fprintf(stderr, "error: \n");
		}
	}
	
	return 0;
}

int read_cmap_file(const char *path, const char *cmap_file, struct cmap *cmap_ptr)
{
	struct file_dscr fd;
	int ret = 0;
	
	char file_path[1024];
	strcpy(file_path, path);
	strcat(file_path, "/");
	strcat(file_path, cmap_file);

	ret = map_file_to_mem(file_path, &fd);
	if (ret == -1) {
		fprintf(stderr, "error: could not map cmap_file to memory\n");
		return -1;
	}

	if (strlen(cmap_file) > 64) {
		fprintf(stderr, "error: cmap file name is too long\n");
		munmap(fd.cont, fd.size);
		return -1;
	}

	strncpy(cmap_ptr->cmap_file_name, cmap_file, 63);

	ret = parse_cmap_file(&fd, cmap_ptr->array, 65536);
	if (ret == -1) {
		fprintf(stderr, "error: error occurred when parsing cmap file\n");
	}
	
	munmap(fd.cont, fd.size);

	return ret;
}

int read_char_map(char *stream, int count, struct char_map_node *node)
{
	int ret = 0;

	node->char_map_count = count;
	node->char_map_array = (struct char_map*)malloc(count * sizeof(struct char_map));
	if (!node->char_map_array) {
		fprintf(stderr, "error: malloc memory failed\n");
		node->char_map_count = 0;
		return -1;
	}

	int i = 0;
	char *stream_end = stream + strlen(stream);
	while (i < count && stream < stream_end) {
		ret = sscanf(stream, "<%hx> <%hx>", &node->char_map_array[i].from, 
				&node->char_map_array[i].to);
		if (ret != 2) {
			fprintf(stderr, "error: read map failed\n");
			return -1;
		}

		i++;
		skip_line(&stream);
		skip_blank_in_string(&stream);
	}

	if (i != count) {
		fprintf(stderr, "error: read incomplete char map\n");
		node->char_map_count = i;
		return -1;
	}

	return 0;
}

int read_range_map(char *stream, int count, struct range_map_node *node)
{
	int ret = 0;

	node->range_map_count = count;
	node->range_map_array = (struct range_map *)malloc(count * sizeof(struct range_map));
	if (!node->range_map_array) {
		fprintf(stderr, "error: malloc memory failed\n");
		node->range_map_count = 0;
		return -1;
	}

	int i = 0;
	char *stream_end = stream + strlen(stream);
	while (i < count && stream < stream_end) {
		ret = sscanf(stream, "<%hx> <%hx> <%hx>", &node->range_map_array[i].begin,
				&node->range_map_array[i].end, &node->range_map_array[i].to);
		if (ret != 3) {
			fprintf(stderr, "error: read range map failed\n");
			node->range_map_count = i;
			return -1;
		}
		
		i++;
		skip_line(&stream);
		skip_blank_in_string(&stream);
	}

	if (i != count) {
		fprintf(stderr, "error: read incomplete char map\n");
		node->range_map_count = i;
		return -1;
	}

	return 0;
}

int parse_tounicode_map(const char *tounicode_stream, struct tounicode_map *map) 
{
	int ret = 0;
	
	/* get the range of this map */
	char *from = NULL;
	char *to = NULL;
	int len = strlen(tounicode_stream);

	const char *bcsr = "begincodespacerange";
	from = (char *)memmem((const void *)tounicode_stream, len, (const void *)(bcsr), strlen(bcsr));
	if (!from) {
		fprintf(stderr, "error: can't get the left range of map\n");
		from = (char *)tounicode_stream;
		return -1;
	}

	from += strlen(bcsr);
	skip_blank_in_string(&from);

	const char *ecsr = "endcodespacerange";
	to = (char *)memmem((const void *)tounicode_stream, len, (const void *)(ecsr), strlen(ecsr));
	if (!to) {
		fprintf(stderr, "error: can't get the right range of map\n");
		to = (char *)tounicode_stream + strlen(tounicode_stream);
		return -1;
	}

	int begin = 0;
	int end = 0;

	ret = sscanf(from, "<%x> <%x>", &begin, &end);
	if (ret != 2) {
		fprintf(stderr, "error: can't get the range of map\n");
		return -1;
	}

//	map->code_range_begin = begin;
//	map->code_range_end = end;

	from = to;
	from += strlen(ecsr);// strlen("endcodespacerange");
	skip_blank_in_string(&from);
	
	int count = 0;

	struct char_map_node *char_node = NULL;
	struct char_map_node *char_map_list_ptr = NULL; 
	struct range_map_node *range_node = NULL;
	struct range_map_node *range_map_list_ptr = NULL;

	/* get the char map and range map*/
	while (strncmp(from, "endcmap", 7)) {
		to = from;
		skip_line(&to);

		/* from 'from' to 'to' is a line */
		if (memmem((const void *)(from), (to - from), (const void *)("beginbfchar"), 11)) {
			/* a char map */
			ret = sscanf(from, "%d", &count);
			if (ret != 1) {
				fprintf(stderr, "error: read number of char map failed\n");
				return -1;
			}
			/* read the map */
			char_node = (struct char_map_node *)malloc(sizeof(struct char_map_node));
			if (!char_node) {
				fprintf(stderr, "error: malloc memory failed\n");
				return -1;
			}
			memset((void *)char_node, 0, sizeof(struct char_map_node));
			skip_line(&from);
			skip_blank_in_string(&from);

			ret = read_char_map(from, count, char_node);
			if (ret == -1) {
				fprintf(stderr, "error: \n");
			}

			int i = 0;
			while (i < char_node->char_map_count) {
				skip_line(&from);
				i++;
			}

			if (char_node->char_map_count == count)
				skip_blank_in_string(&from); // "endbfchar"

			skip_blank_in_string(&from);

			char_node->next = NULL;

			if (!map->char_map_list)
				map->char_map_list = char_node;
			else {
				char_map_list_ptr = map->char_map_list;

				while (char_map_list_ptr->next)
					char_map_list_ptr = char_map_list_ptr->next;
				char_map_list_ptr->next = char_node;
			}
		}
		else if (memmem((const void *)(from), (to - from), (const void *)("beginbfrange"), 12)) {
			/* a range map */
			ret = sscanf(from, "%d", &count);
			if (ret != 1) {
				fprintf(stderr, "error: read number of range map failed\n");
				return -1;
			}
			/* read the map */
			range_node = (struct range_map_node *)malloc(sizeof(struct range_map_node));
			if (!range_node) {
				fprintf(stderr, "error: malloc memory failed\n");
				return -1;
			}

			memset((void *)range_node, 0, sizeof(struct range_map_node));

			skip_line(&from);
			skip_blank_in_string(&from);

			ret = read_range_map(from, count, range_node);
			if (ret == -1) {
				fprintf(stderr, "error: \n");
			}

			int i = 0;
			while (i < range_node->range_map_count) {
				skip_line(&from);
				i++;
			}

			if (range_node->range_map_count == count)
				skip_blank_in_string(&from); // "endbfrange"
			skip_blank_in_string(&from);

			range_node->next = NULL;
			if (!map->range_map_list) {
				map->range_map_list = range_node;
			}
			else {
				range_map_list_ptr = map->range_map_list;
				while (range_map_list_ptr->next)
					range_map_list_ptr = range_map_list_ptr->next;
				range_map_list_ptr->next = range_node;
			}
		}
		else {
			/* unknown */
			skip_line(&from);
		}
	}
	
	return 0;
}

int get_unicode_value(int code, struct tounicode_map *map)
{
//	if (code < map->code_range_begin || code > map->code_range_end)
//		return -1;

	int i, count;

	struct char_map_node *char_map_ptr = map->char_map_list;
	struct char_map *char_array;
	while (char_map_ptr) {
		if (code >= char_map_ptr->char_map_array[0].from && 
				code <= char_map_ptr->char_map_array[char_map_ptr->char_map_count - 1].from) {
			/* get the value */
			i = 0;
			count = char_map_ptr->char_map_count;
			char_array = char_map_ptr->char_map_array;
			for (i = 0; i < count; i++) {
				if (char_array[i].from == code)
					return char_array[i].to;
				if (char_array[i].from > code)
					break;
			}
		}

		char_map_ptr = char_map_ptr->next;
	}

	struct range_map_node *range_map_ptr = map->range_map_list;
	struct range_map *range_array = NULL;
	while (range_map_ptr) {
		if (code >= range_map_ptr->range_map_array[0].begin &&
				code <= range_map_ptr->range_map_array[range_map_ptr->range_map_count - 1].end) {
			/* get the value */
			i = 0;
			count = range_map_ptr->range_map_count;
			range_array = range_map_ptr->range_map_array;
			for (i = 0; i < count; i++) {
				if (range_array[i].begin <= code &&
						range_array[i].end >= code)
					return (code + range_array[i].to - range_array[i].begin);
				if (range_array[i].begin > code)
					break;
			}
		}

		range_map_ptr  = range_map_ptr->next;
	}

	return -1;
}

/* for test */
void print2(struct xref_sect *ptr)
{
	while (ptr) {
		printf("prev = %d\n", ptr->trailer.prev);

		if (ptr->trailer.id) 
			printf("id = %s\n", ptr->trailer.id);
		
		if (ptr->trailer.root) {
			printf("root: id = %d, gen = %d, status = %c\n", 
					ptr->trailer.root->obj_num, ptr->trailer.root->gen, ptr->trailer.root->keyword);
		}
		if (ptr->trailer.info) {
			printf("info: id = %d, gen = %d, status = %c\n", 
					ptr->trailer.info->obj_num, ptr->trailer.info->gen, ptr->trailer.info->keyword);
		}
		struct xref_item_body *body = ptr->body;
		while (body) {
			printf("start = %d\n", body->start);
			printf("count = %d\n", body->count);
			int i = 0;
			for (;i < body->count; i++) {
				printf("%d : offset = %d, gen = %d, status = %c\n", i + body->start, 
						body->array[i].offset, body->array[i].gen, body->array[i].status);
			}
			body = body->next;
		}

		ptr = ptr->next;
	}
}

int read_object(struct file_dscr *fd, int obj_begin, obj_disc *obj)
{
	int ret = 0;

	if (obj_begin >= fd->size) {
		fprintf(stderr, "error: read beyond the boundary of file\n");
		return -1;
	}

	char *buf_ptr = (char *)(fd->cont + obj_begin);

	skip_blank_in_string(&buf_ptr);
	if (!isdigit(*buf_ptr)) {
		fprintf(stderr, "error: object should begin with a number\n");
		return -1;
	}

	int num, start;
	ret = sscanf(buf_ptr, "%d%dobj", &num, &start);
	if (ret != 2) {
		fprintf(stderr, "error: error occurred when reading num from file\n");
		return -1;
	}

	void *begin = (void *)buf_ptr;
	void *end = NULL;
	size_t left = (fd->cont + fd->size - begin);

	/* find the end of the object */
	end = memmem((const void *)begin, left, (const void *)("endobj"), 6);
	if (!end) {
		fprintf(stderr, "error: can't find the end of object\n");
		return -1;
	}

	/* have found the end of obj */
	obj->size = end + 6 - begin;
	obj->cont = begin;

	return 0;
}

int read_name(char *file_buf, char *buf, const int name_len_max)
{
//	const int name_len_max = 128; /* limitation of a name string (in PDF ref 1.7) */
	int index = 0;
	char *buf_ptr = file_buf;
	char *start = buf_ptr;
	if (*buf_ptr == '/') {
		buf[index++] = *buf_ptr;
		while (index < name_len_max) {
			buf_ptr++;
			if (*buf_ptr == '#') {
				/* in hex mod */
				buf[index++] = (char)((int)(*(buf_ptr + 1)) << 4) | (int)(*(buf_ptr + 2));
				buf_ptr += 2;
			}
			else if (*buf_ptr == '\t' || *buf_ptr == '\n' || *buf_ptr == ' ' ||
					*buf_ptr == '\0' || *buf_ptr == '\014' || *buf_ptr == '\r') {
				/* white-space char */
				break;
			}
			else if (*buf_ptr == '(' || *buf_ptr == ')' || *buf_ptr == '<' || *buf_ptr == '>' ||
					*buf_ptr == '[' || *buf_ptr == ']' || *buf_ptr == '{' || *buf_ptr == '}') {
				/* delimiter char */
				/* back to last char */
				buf_ptr--;
				break;
			}
			else if (*buf_ptr == '/') {
				/* next is also a name object */
				/* back to last char */
				buf_ptr--;
				break;
			}
			else if (*buf_ptr == '%') {
				/* the following is comment, skip them */
				/* back to last char */
				buf_ptr--;
				break;
			}
			else {
				buf[index++] = *buf_ptr;
			}
		}
		buf[index] = '\0';
        buf[name_len_max - 1] = '\0';
		return buf_ptr - start + 1;
	}
	else {
		/* error */
		fprintf(stderr, "error: the first char in Name Object should be '/'\n");
		return -1;
	}
}

int get_array_obj_len(int *len, const char *file_buf)
{
	if (*file_buf != '[') {
		fprintf(stderr, "error: could not find '[' in array's front\n");
		return -1;
	}

	char *buf_end = (char *)file_buf;
	int i = 0;
	/*
	 * Assuming that length of array object is less than 1024, preventing
	 * from reading the end of file if ']' is missing.
	 */
	while (*buf_end != ']' && i++ < 1024) {
		buf_end++;
	}

	if (*buf_end != ']') {
		fprintf(stderr, "error: could not find ']' at array's end\n");
		return -1;
	}

	/* including '[' and ']' */
	*len = buf_end + 1 - file_buf;

	return 0;
}

/* 
 * This function reads cross-reference table from file.
 */
int read_xref(struct file_dscr *fd, off_t fileOffset, struct xref_sect **list_addr) 
{
	int ret = 0;
	/* the length limit of a name object is 127, according to PDF Ref 1.7 */

	char *fileBufp = (char *)(fd->cont + fileOffset);
	if (strncmp((const char *)fileBufp, "xref", 4)) {
		fprintf(stderr, "error: could not read xref here\n");
		return -1;
	}

	/* skip "xref" */
	fileBufp += 4;
	
	*list_addr = (struct xref_sect *)malloc(sizeof(struct xref_sect));
	if (!(*list_addr)) {
		fprintf(stderr, "error: malloc memory failed\n");
		return -1;
	}
	memset((void *)(*list_addr), 0, sizeof(struct xref_sect));
	
	int start, count;
	struct xref_item_body *body = NULL;
	skip_blank_in_string(&fileBufp);

	while (isdigit(*fileBufp)) {
		ret = sscanf(fileBufp, "%d%d", &start, &count);
		if (ret != 2) {
			fprintf(stderr, "read error\n");
			ret = -1;
			goto read_xref_end;
		}
		skip_line(&fileBufp);
		if (!((*list_addr)->body)) {
			(*list_addr)->body = (struct xref_item_body *)malloc(sizeof(struct xref_item_body));
			if (!((*list_addr)->body)) {
				fprintf(stderr, "error: malloc memory failed\n");
				ret = -1;
				goto read_xref_end;
			}

            memset((*list_addr)->body, 0, sizeof(struct xref_item_body));
			body = (*list_addr)->body;
		}
		else {
			body = (*list_addr)->body;
			while (body->next) 
				body = body->next;
			body->next = (struct xref_item_body *)malloc(sizeof(struct xref_item_body));
			if (!(body->next)) {
				fprintf(stderr, "error: malloc memory failed\n");
				ret = -1;
				goto read_xref_end;
			}

            memset(body->next, 0, sizeof(struct xref_item_body));
			body = body->next;
		}
		body->start = start;
		body->count = count;
		body->next = NULL;
		if (count != 0) {
			body->array = (struct xref_item *)malloc(count * sizeof(struct xref_item));
			if (!(body->array)) {
				fprintf(stderr, "error: malloc memory failed\n");
				ret = -1;
				goto read_xref_end;
			}
		}
		skip_blank_in_string(&fileBufp);

		int i = 0;
		int offset;
		uint16_t gen;
		char status;
		while (i < count) {
			sscanf(fileBufp, "%d %hd %c", &offset, &gen, &status);
			body->array[i].offset = offset;
			body->array[i].gen = gen;
			body->array[i].status = status;

			/* the length of every item is 20, according to PDF Ref 1.7 */
			fileBufp += 20;
			i++;
		}
	}
	/* find if there's a trailer entry. */
	if (memcmp((const void *)fileBufp, (const void *)("trailer"), 7)) {
		fprintf(stderr, "error: it should be `trailer' here\n");
		ret = -1;
		goto read_xref_end;
	}
	fileBufp += 7;
	/* read the dictionary and store all the entries */
	char *dict = fileBufp;
	char *dictEnd = (char *)memmem((const void *)fileBufp, 
							(size_t)(fd->cont + fd->size - (void *)fileBufp),
							(const void *)(">>"), 2);
	if (!dictEnd) {
		fprintf(stderr, "error: incomplete dictionary\n");
		ret = -1;
		goto read_xref_end;
	}
	char *str = NULL;
	size_t dictSize = dictEnd - dict;
	
	skip_blank_in_string(&dict);

	str = (char *)memmem((const void *)dict, dictSize, (const void *)("/Size"), 5);
	if (str) {
		ret = sscanf(str, "/Size%d", &((*list_addr)->trailer.size));
		if (ret != 1) {
			printf("error\n");
		}
	}

	str = (char *)memmem((const void *)dict, dictSize, (const void *)("/Prev"), 5);
	if (str) {
		ret = sscanf(str, "/Prev%d", &((*list_addr)->trailer.prev));
		if (ret != 1) {
			printf("error\n");
		}
	}

	str = (char *)memmem((const void *)dict, dictSize, (const void *)("/Root"), 5);
	if (str) {
		/* read the XrefItem of Root */
		str += 5;
		(*list_addr)->trailer.root = (struct indirect_obj *)malloc(sizeof(struct indirect_obj));
		if (!((*list_addr)->trailer.root)) {
			fprintf(stderr, "error: malloc memory failed\n");
		}
		struct indirect_obj *root = (*list_addr)->trailer.root;
		ret = sscanf(str, "%d %hd %c", &(root->obj_num), &(root->gen), &(root->keyword));
		if (ret != 3) {
			fprintf(stderr, "error:\n");
		}
	}

	str = (char *)memmem((const void *)dict, dictSize, (const void *)("/XRefStm"), 8);
	if (str) {
		/* TODO: only exists in file whose version is greater than 1.4. */
		// printf("XRefXtm only exists in pdf whose version is greater than 1.4, to be implemented\n");
	}

	str = (char *)memmem((const void *)dict, dictSize, (const void *)("/Encrypt"), 8);
	if (str) {
		/* TODO: encrypted file, to be implement */
		fprintf(stderr, "error: Reading from encrypted file is still not implemented\n");
		ret = -1;
		goto read_xref_end;
	}

	str = (char *)memmem((const void *)dict, dictSize, (const void *)("/Info"), 5);
	if (str) {
		str += 5;
		(*list_addr)->trailer.info = (struct indirect_obj *)malloc(sizeof(struct indirect_obj));
		struct indirect_obj *info = (*list_addr)->trailer.info;
		ret = sscanf(str, "%d %hd %c", &(info->obj_num), &(info->gen), &(info->keyword));
		if (ret != 3) {
			fprintf(stderr, "error\n");
		}
	}

/* 	str = (char *)memmem((const void *)dict, dictSize, (const void *)("/ID"), 3);
 * 	if (str) {
 * 		str += 3;
 * 		//  read the array object 
 * 		read_array_obj(&((*list_addr)->trailer.id), str);
 * 	}
 */
	// no use
	(*list_addr)->trailer.id = NULL;

read_xref_end:
	if (ret == -1) {
		free_xref_list(*list_addr);
	}

	return ret;
}

int read_all_xrefs(struct file_dscr *fd, struct xref_sect **list_addr)
{
	int ret = 0;

	/*
	 * Acrobat viewers require only that the %%EOF marker appear somewhere
	 * within the last 1024 bytes of the file. (PDF ref P1102)
	 */
	int32_t offset;
	int32_t length = 1024;
	*list_addr = NULL;
	struct xref_sect *tail = NULL, *ptr = NULL;

	char *fileBufp = (char *)(fd->cont + fd->size - length);

	char *startxref = (char *)memmem(fileBufp, (size_t)length, "startxref", 9);
	if (!startxref) {
		fprintf(stderr, "error: can't find startref\n");
		return -1;
	}

	/* skip the "startxref" string */
	startxref += 9;
	ret = sscanf((const char *)startxref, "%d", &offset);
	if (ret != 1) {
		printf("error:\n");
		return -1;
	}
	
	while (offset != 0) {
		if (offset < 0 || offset > fd->size) {
			fprintf(stderr, "error: offset beyond the boundary of file\n");
			return -1;
		}
		ret = read_xref(fd, offset, &ptr);
		if (ret == -1) {
			return -1;
		}

		if (*list_addr) {
			tail->next = ptr;
			tail = tail->next;
		}
		else {
			*list_addr = tail = ptr;
		}

		offset = ptr->trailer.prev;
	}
	
	return 0;
}

/*
 * get Object Offset according to obj_num
 */
int get_obj_offset(int obj_num, struct xref_sect *list)
{
	struct xref_item_body *body = NULL;
	int start = 0;
	int count = 0;
	while (list) {
		body = list->body;
		while (body) {
			start = body->start;
			count = body->count;
			if (obj_num >= start && obj_num < (start+count)) {
				return body->array[obj_num - start].offset;
			}
			body = body->next;
		}

		list = list->next;
	}

	return -1;
}

/*
 * find the offset of Catalog Object
 */
int get_catalog_offset(struct xref_sect *list)
{
	int root_obj_num = 0;
	struct xref_sect *ptr = list;
	while (ptr) {
		if (ptr->trailer.root) {
			root_obj_num = ptr->trailer.root->obj_num;
			break;
		}
		ptr = ptr->next;
	}

	if (root_obj_num == 0) {
		fprintf(stderr, "error: can't find the reference of Catalog Object\n");
		return -1;
	}

	return get_obj_offset(root_obj_num, list);
}

/*
 * find the Pages Reference in Catalog Object, and then look up
 * the offset of Pages Object in the Catalog Object according to
 * the reference.
 */
int get_pages_obj_offset(obj_disc *catalog, struct xref_sect *list)
{
	int ret = 0;
	char *parm = NULL;

	parm = (char *)memmem(catalog->cont, catalog->size, (const void *)("/Catalog"), 8);
	if (!parm) {
		fprintf(stderr, "error: this object is not Catalog\n");
		return -1;
	}

	parm = (char *)memmem(catalog->cont, catalog->size, (const void *)("/Pages"), 6);
	if (!parm) {
		fprintf(stderr, "error: can't find Pages reference in Catalog\n");
		return -1;
	}

	int pages_obj_num = 0;
	short gen = 0;
	char r = 0;

	ret = sscanf(parm, "/Pages %d %hd %c", &pages_obj_num, &gen, &r);
	if (ret < 3) {
		fprintf(stderr, "error: read Pages reference error in Catalog\n");
		return -1;
	}

	if (r != 'R') {
		fprintf(stderr, "error: read Pages reference error in Catalog\n");
		return -1;
	}

	int offset = get_obj_offset(pages_obj_num, list);
	if (offset == -1) {
		fprintf(stderr, "error: can't find pages object offset in the list\n");
	}

	return offset;
}

enum font_subtype get_font_subtype(char *subtype_name)
{
	if (!strncmp(subtype_name, "/Type1", 6)) {
		return Type1;
	}
	else if (!strncmp(subtype_name, "/TrueType", 9)) {
		return TrueType;
	}
	else if (!strncmp(subtype_name, "/Type3", 6)) {
		return Type3;
	}
	else if (!strncmp(subtype_name, "/CIDFontType0", 13)) {
		return CIDFontType0;
	}
	else if (!strncmp(subtype_name, "/CIDFontType2", 13)) {
		return CIDFontType2;
	}
	else if (!strncmp(subtype_name, "/Type0", 6)) {
		return Type0;
	}
	else if (!strncmp(subtype_name, "/MMType1", 8)) {
		return MMType1;
	}
	else {
		return UNKNOWN_FONTTYPE;
	}
}

struct font *parse_font(struct file_dscr *fd, char *font_name, obj_disc *font_obj, struct xref_sect *list)
{
	int ret = 0;

	struct font *font_node = NULL;
	char *font_buf = (char *)(font_obj->cont);

	font_buf = (char *)memmem(font_obj->cont, font_obj->size, (const void *)("/Type"), 5);
	if (!font_buf) {
		fprintf(stderr, "error: font type\n");
		return NULL;
	}

	font_node = (struct font *)malloc(sizeof(struct font));
	if (!font_node) {
		fprintf(stderr, "error: malloc memory failed\n");
		return NULL;
	}
    memset(font_node, 1, sizeof(struct font));

	font_node->next = NULL;

	font_buf += 5;
	skip_blank_in_string(&font_buf);

	strncpy(font_node->font_name, font_name, 19);
	font_node->font_name[19] = '\0';

	font_buf = (char *)memmem(font_obj->cont, font_obj->size, (const void *)("/Subtype"), 8);
	if (!font_buf) {
		fprintf(stderr, "error: subtype needed\n");
		free(font_node);
		return NULL;
	}

	font_buf += 8;
	skip_blank_in_string(&font_buf);

	char subtype_buf[128];
	ret = read_name(font_buf, subtype_buf, 128);

	if (ret == -1) {
		fprintf(stderr, "error: read name error\n");
		free(font_node);
		return NULL;
	}

	enum font_subtype subtype = get_font_subtype(subtype_buf);
	if (subtype == UNKNOWN_FONTTYPE) {
		fprintf(stderr, "error: unknown subtype\n");
		free(font_node);
		return NULL;
	}
	
	font_node->subtype = subtype;

	font_node->encoding_name = NULL;
	font_node->tounicode = NULL;

	if (subtype == Type1 || subtype == MMType1 || \
            subtype == Type3 || subtype == TrueType) {
		return font_node;
	}

	/* get encoding_name */
	int obj_num = 0;
	unsigned short gen = 0;
	char r = '\0';

	font_buf = (char *)memmem(font_obj->cont, font_obj->size, (const void *)("/Encoding"), 9);
	if (!font_buf) {
		font_node->encoding_name = NULL;
	}
	else {
		font_buf += 9;
		skip_blank_in_string(&font_buf);
		/* read the encoding name */
		if (is_name_obj(font_buf)) {
			font_node->encoding_name = (char *)malloc(64);
			if (!font_node->encoding_name) {
				fprintf(stderr, "error: malloc memory failed\n");
			}
			else {
				ret = read_name(font_buf, font_node->encoding_name, 64);
                if (ret == -1) {
                    free(font_node->encoding_name);
                    font_node->encoding_name = NULL;
                }
                else {
                    font_node->encoding_name[63] = '\0';
                }
			}
		}
        else {
            font_node->encoding_name = NULL;
        }
	}

	font_buf = (char *)memmem(font_obj->cont, font_obj->size, (const void *)("/ToUnicode"), 10);
	if (!font_buf) {
		font_node->tounicode = NULL;
		return font_node;
	}

	/* read tounicode obj num */
	font_buf += 10;
	skip_blank_in_string(&font_buf);
	ret = sscanf(font_buf, "%d %hd %c", &obj_num, &gen, &r);
	if (ret != 3 || r != 'R') {
		/* seems to be read wrongly */
		fprintf(stderr, "error: can't read reference here\n");
		font_node->tounicode = NULL;
		return font_node;
	}

	int offset = -1;
	offset = get_obj_offset(obj_num, list);
	if (offset == -1) {
		fprintf(stderr, "error: can't find offset of the object\n");
		font_node->tounicode = NULL;
		return font_node;
	}
	obj_disc tounicode_stream;

	ret = read_object(fd, offset, &tounicode_stream);
	if (ret == -1) {
		fprintf(stderr, "error: can't read the object\n");
		font_node->tounicode = NULL;
		return font_node;
	}

	char *stream = NULL;
	int len = 0;
	ret = decode(&tounicode_stream, &stream, &len, list);
	if (ret == -1) {
		font_node->tounicode = NULL;
		return font_node;
	}

	font_node->tounicode = (struct tounicode_map *)malloc(sizeof(struct tounicode_map));
	if (!font_node->tounicode) {
		fprintf(stderr, "error: malloc memory failed\n");
		free(stream);
		return font_node;
	}

	/* XXX: for test */

//	FILE *fp = fopen("tounicode.txt", "w+");
//	fwrite(stream, strlen(stream), 1, fp);
//	fclose(fp);

	/* XXX */
	memset(font_node->tounicode, 0, sizeof(struct tounicode_map));
	ret = parse_tounicode_map(stream, font_node->tounicode);
	free(stream);

	if (ret == -1) {
		fprintf(stderr, "error: parse tounicode error\n");
		if (!font_node->tounicode->char_map_list && !font_node->tounicode->range_map_list) {
			free(font_node->tounicode);
			font_node->tounicode = NULL;
		}
	}

	return font_node;
}

struct font *get_font_list(struct file_dscr *fd, char *font_buf, size_t size, struct xref_sect *list)
{
	int ret = 0;

	struct font *font_list = NULL;
	struct font *font_tail = NULL;
	struct font *font_ptr = NULL;

	char *font_buf_end = font_buf + size;

	skip_blank_in_string(&font_buf);
	char font_name[128];

	if (*font_buf != '/') {
		fprintf(stderr, "error: read font failed\n");
		return NULL;
	}

	int obj_num;
	unsigned short gen;
	char r;

	int offset;
	obj_disc font_obj;

	while (font_buf < font_buf_end) {
		ret = read_name(font_buf, font_name, 128);
		if (ret == -1) {
			fprintf(stderr, "error: read font name failed\n");
			return font_list;
		}

		font_buf += strlen(font_name);
		skip_blank_in_string(&font_buf);

		ret = sscanf(font_buf, "%d %hd %c", &obj_num, &gen, &r);
		if (ret != 3 || r != 'R') {
			fprintf(stderr, "error: read font reference failed\n");
			return font_list;
		}
		
		offset = get_obj_offset(obj_num, list);
		if (offset == -1) {
			fprintf(stderr, "error: \n");
			/* could not return from here */
		}
		else {
			ret = read_object(fd, offset, &font_obj);
			if (ret == -1) {
				fprintf(stderr, "error: \n");
				/* could not return from here */
			}
			else {
				font_ptr = parse_font(fd, font_name, &font_obj, list);
				if (font_ptr) {
					/* we get the font */
					if (font_list) {
						font_tail->next = font_ptr;
						font_tail = font_tail->next;
					}
					else {
						font_list = font_tail = font_ptr;
					}
				}
			}
		}
		/* forward to next font */
		font_buf = (char *)memchr((void *)font_buf, (int)('R'), font_buf_end - font_buf);
		if (!font_buf) {
			fprintf(stderr, "error: \n");
			return font_list;
		}

		font_buf++; // skip 'R'
		skip_blank_in_string(&font_buf);
	}

	return font_list;
}

int free_font_list(struct font *font_list) 
{
	struct font *font_list_ptr = NULL;
	while ((font_list_ptr = font_list)) {
		font_list = font_list->next;
		/* free inner resources */
		if (font_list_ptr->encoding_name)
			free(font_list_ptr->encoding_name);
		if (font_list_ptr->tounicode) {
			if (font_list_ptr->tounicode->char_map_list) {
				struct char_map_node *char_map_list = NULL;
				struct char_map_node *char_map_ptr = NULL;

				char_map_list = font_list_ptr->tounicode->char_map_list;
				while ((char_map_ptr =  char_map_list)) {
					char_map_list = char_map_list->next;
					if (char_map_ptr->char_map_array)
						free(char_map_ptr->char_map_array);
					free(char_map_ptr);
				}

			}
			if (font_list_ptr->tounicode->range_map_list) {
				struct range_map_node *range_map_list = NULL;
				struct range_map_node *range_map_ptr = NULL;

				range_map_list = font_list_ptr->tounicode->range_map_list;
				while ((range_map_ptr = range_map_list)) {
					range_map_list = range_map_list->next;
					if (range_map_ptr->range_map_array)
						free(range_map_ptr->range_map_array);
					free(range_map_ptr);
				}
			}
			free(font_list_ptr->tounicode);
		}
		free(font_list_ptr);
	}

	return 0;
}

struct font *get_font_resources(struct file_dscr *fd, obj_disc *page, struct xref_sect *list)
{
	int ret = 0;

	int parent_obj_num = -1, child_obj_num = -1;
	char *font = NULL,
		 *font_end = NULL;
	struct font *font_list = NULL;

	obj_disc page_tmp;
	page_tmp.cont = page->cont;
	page_tmp.size = page->size;

	font = (char *)memmem(page_tmp.cont, page_tmp.size, (const void *)("/Font"), 5);
	if (!font) {
		char *resource = (char *)memmem(page_tmp.cont, page_tmp.size, \
				(const void *)("/Resources"), 10);
		if (!resource) {
			goto parent_font;
		}

		resource += 10;	
		skip_blank_in_string(&resource);

		obj_disc resource_obj;
		int resource_offset = -1;

		int page_obj_num = -1;
		unsigned short gen = 0;
		char r = '\0';

		ret = sscanf(resource, "%d %hd %c", &page_obj_num, &gen, &r);
		if (ret != 3) {
			/* can't read the parent's offset */
			fprintf(stderr, "error: can't read the reference\n");
			goto parent_font;
		}

		if (r != 'R') {
			/* it's not a reference */
			fprintf(stderr, "error: this is not a object reference\n");
			goto parent_font;
		}

		if (page_obj_num == -1) {
			/* can't find the resource object reference */
			fprintf(stderr, "error: can't find the resource object reference\n");
			goto parent_font;
		}

		resource_offset = get_obj_offset(page_obj_num, list);
		if (resource_offset == -1) {
			/* can't find the resource object */
			fprintf(stderr, "error: can't find the resource object\n");
			goto parent_font;
		}

		ret = read_object(fd, resource_offset, &resource_obj);
		
		if (ret == -1) {
			fprintf(stderr, "err: cannot find the resource object "
						"according to the offset.\n");
			goto parent_font;
		}

		font = (char *)memmem(resource_obj.cont, resource_obj.size, \
				(const void *)("/Font"), 5);
		if (!font) {
			goto parent_font;
		}

		/* skip the "/Font" */
		font += 5;
		skip_blank_in_string(&font);
		/* skip the "<<" */
		font += 2;

		size_t size = resource_obj.cont + resource_obj.size - (void *)font;

		font_end = (char *)memmem((const void *)font, size, (const void *)(">>"), 2);
		if (!font_end) {
			fprintf(stdout, "no font end found\n");
			goto parent_font;
		}
		else {
			size = font_end - font;
			font_list = get_font_list(fd, font, size, list);
			return font_list;
		}
	}

parent_font:
	// no fonts have been found!
	while (!font) {
		/* 
		 * if no font has been found, and this page has a parent entry,
		 * try to find font resource in the parent page, and 
		 * just recursively find until it has no either font
		 * or parent entry.
		 */
		char *parent = (char *)memmem(page_tmp.cont, page_tmp.size, (const void *)("/Parent"), 7);
		if (!parent) {
			/* has no parent entry */
			return NULL;
		}

		parent += 7;
		skip_blank_in_string(&parent);

		int page_obj_num = -1;
		unsigned short gen = 0;
		char r = '\0';

		int page_offset = -1;
		ret = sscanf(parent, "%d %hd %c", &page_obj_num, &gen, &r);
		if (ret != 3) {
			/* can't read the parent's offset */
			fprintf(stderr, "error: can't read the reference\n");
			return NULL;
		}

		if (r != 'R') {
			/* it's not a reference */
			fprintf(stderr, "error: this is not a parent reference\n");
			return NULL;
		}

		if (page_obj_num == -1) {
			/* can't find the page object reference */
			fprintf(stderr, "error: can't find the page object reference\n");
			return NULL;
		}

		page_offset = get_obj_offset(page_obj_num, list);
		if (page_offset == -1) {
			/* can't find the page object */
			fprintf(stderr, "error: can't find the page object\n");
			return NULL;
		}

		ret = read_object(fd, page_offset, &page_tmp);
        
        if (ret == -1) {
            return NULL;
        }

		parent_obj_num = page_obj_num;

        /* 
		 * Check wether parent's obj num and child's obj num to 
		 * avoid endless loop.
		 *
		 * Here is an example.
		 * 2 0 obj
		 * <<
		 *	blah ...
		 * >>
		 *
		 * 3 0 obj
		 * <<
		 *  blah ...
		 * >>
		 * Parent 2 0 R
		 * end obj
		 *
		 * 2 0 obj is parent of 3 0 obj, however, 2 0 obj doesn't have an end flag.
		 * In 3 0 obj, we could not find any font info. Therefore, we turn to its 
		 * parent - 2 0 obj to find font info. Since 2 0 obj has no end flag, we may 
		 * take 2 0 obj and 3 0 obj as an object. Program will loop endlessly in 
		 * this object to find font info. 
         */

		if (parent_obj_num != -1 && child_obj_num != -1 &&
				parent_obj_num == child_obj_num) {
			return NULL;
		}

		child_obj_num = parent_obj_num;

        font = (char *)memmem(page_tmp.cont, page_tmp.size, (const void *)("/Font"), 5);
	}

	if (!font)
		return NULL;

	/* skip the "/Font" */
	font += 5;
	skip_blank_in_string(&font);
	/* skip the "<<" */
	font += 2;

	size_t size = page_tmp.cont + page_tmp.size - (void *)font;

	font_end = (char *)memmem((const void *)font, size, (const void *)(">>"), 2);
	if (!font_end) {
		fprintf(stdout, "no font end found\n");
	}
	else {
		size = font_end - font;
		font_list = get_font_list(fd, font, size, list);
	}

	return font_list;
}

int flate_decode(obj_disc *content, obj_disc *result)
{
	int ret = 0;

	uLong in_len = content->size;
	uLongf out_len = in_len * 10;
	const Bytef *in_buf = (const Bytef *)(content->cont);
	Bytef *out_buf = (Bytef *)malloc(out_len);
	if (!out_buf) {
		fprintf(stderr, "error: malloc memory failed\n");
		result->cont = NULL;
		result->size = 0;
		return -1;
	}

	ret = uncompress(out_buf, &out_len, in_buf, in_len);
	if (ret < 0) {
		fprintf(stderr, "error: uncompress failed\n");
		result->cont = NULL;
		result->size = 0;
		free(out_buf);
		return -1;
	}

	result->size = out_len;
	result->cont = out_buf;
/* 	if (!result->cont) {
 * 		fprintf(stderr, "error: realloc memory failed\n");
 * 		result->size = 0;
 * 		return -1;
 * 	}
 */
	return 0;
}

int unfilter(obj_disc *content, obj_disc *result, const char *filter)
{
	int ret = 0;

	if (!strncmp(filter, "/FlateDecode", 12)) {
		ret = flate_decode(content, result);
		return ret;
	}
	else if (!strncmp(filter, "/ASCIIHexDecode", 15)) {
		/* TODO: seldom used in pdf, to be implemented */
		result->cont = NULL;
		result->size = 0;
	}
	else if (!strncmp(filter, "/ASCII85Decode", 14)) {
		/* TODO: seldom used, to be implemented */
		result->cont = NULL;
		result->size = 0;
	}
	else if (!strncmp(filter, "/LZWDecode", 10)) {
		/* TODO: seldom used, to be implemented */
		result->cont = NULL;
		result->size = 0;
	}
	else if (!strncmp(filter, "/RunLengthDecode", 16)) {
		/* TODO: seldom, to be implemented */
		result->cont = NULL;
		result->size = 0;
	}
	else if (!strncmp(filter, "/CCITTFaxDecode", 15)) {
		/* image compress filter */
		/* could be ignored */
		result->cont = NULL;
		result->size = 0;
	}
	else if (!strncmp(filter, "/JBIG2Decode", 12)) {
		/* image compress filter */
		/* could be ignored */
		result->cont = NULL;
		result->size = 0;
	}
	else if (!strncmp(filter, "/DCTDecode", 10)) {
		/* image compress filter */
		/* could be ignored */
		result->cont = NULL;
		result->size = 0;
	}
	else if (!strncmp(filter, "/JPXDecode", 10)) {
		/* image compress filter */
		/* could be ignored */
		result->cont = NULL;
		result->size = 0;
	}
	else if (!strncmp(filter, "/Crypt", 6)) {
		/* encrypted compress filter */
		/* should be ignored */
		result->cont = NULL;
		result->size = 0;
	}
	else {
		fprintf(stderr, "error: unknown filter\n");
		result->cont = NULL;
		result->size = 0;
	}

	return -1;
}

int decode(obj_disc *content, char **uncmpr_addr, int *uncmpr_len, struct xref_sect *list)
{
	int ret = 0;
	
	char *stream = (char *)memmem(content->cont, content->size, (const void *)("stream"), 6);
	char *stream_end = (char *)memmem(content->cont, content->size, (const void *)("endstream"), 9);

	if (!stream || !stream_end) {
		fprintf(stderr, "can't find stream in the content stream\n");
		*uncmpr_addr = NULL;
		*uncmpr_len = 0;
		return -1;
	}

	stream += 6;
	skip_blank_in_string(&stream);

	if (stream > stream_end) {
		fprintf(stderr, "error: can't find stream in the content stream\n");
		*uncmpr_addr = NULL;
		*uncmpr_len = 0;
		return -1;
	}

	char *decode = (char *)memmem(content->cont, content->size, (const void *)("/Filter"), 7);
	if (!decode) {
		/*
		 * no decode filter found, just return the stream 
		 */
		*uncmpr_len = stream_end - stream;
		*uncmpr_addr = (char *)malloc(*uncmpr_len);
		if (!(*uncmpr_addr)) {
			fprintf(stderr, "error: malloc memory failed\n");
			*uncmpr_addr = NULL;
			*uncmpr_len = 0;
			return -1;
		}

		memcpy((void *)(*uncmpr_addr), (const void *)stream, (size_t)(*uncmpr_len));

		return 0;
	}
	else {
		decode += 7;
		skip_blank_in_string(&decode);
		if (*decode == '/') {
			char unfilter_name[128];
			ret = read_name(decode, unfilter_name, 128);
			if (ret == -1) {
				fprintf(stderr, "error: read filter name failed\n");
				*uncmpr_addr = NULL;
				*uncmpr_len = 0;

				return -1;
			}

			obj_disc cmpr_stream;
			obj_disc result;

			cmpr_stream.cont = stream;
			cmpr_stream.size = stream_end - stream;

			ret = unfilter(&cmpr_stream, &result, unfilter_name);
			if (ret == -1) {
				fprintf(stderr, "error: uncompress the stream failed\n");
				*uncmpr_addr = NULL;
				*uncmpr_len = 0;

				return -1;
			}

			*uncmpr_len = result.size;
			*uncmpr_addr = (char *)(result.cont);

			return 0;
		}
		else if (*decode == '[') {
			/* 
			 * it's a decode array, just read the decode filter one by 
			 * one, and decode the stream according to the filter name.
			 */
			int len = 0;
			ret = get_array_obj_len(&len, (const char *)(decode));
			if (ret == -1) {
				*uncmpr_addr = NULL;
				*uncmpr_len = 0;
				return -1;
			}
			char *filter_array = decode, 
				 *filter_array_end = decode + len;

			obj_disc cmpr_stream;
			obj_disc result;

            cmpr_stream.cont = NULL;
            cmpr_stream.size = 0;

			result.cont = stream;
			result.size = stream_end - stream;

			char unfilter_name[128];
			filter_array += 1; // skip '['
			filter_array_end -= 1; // let *filter_array_end to be ']'
			skip_blank_in_string(&filter_array);
			while (filter_array < filter_array_end) {
				if (cmpr_stream.cont && cmpr_stream.cont != content->cont)
					free(cmpr_stream.cont);

				cmpr_stream.cont = result.cont;
				cmpr_stream.size = result.size;
				result.cont = NULL;
				result.size = 0;

				ret = read_name(filter_array, unfilter_name, 128);
				if (ret == -1) {
					fprintf(stderr, "error: read filter name failed\n");
					*uncmpr_addr = NULL;
					*uncmpr_len = 0;
					return -1;
				}

				ret = unfilter(&cmpr_stream, &result, unfilter_name);
				if (ret == -1) {
					fprintf(stderr, "error: uncompress the stream failed\n");

					if (cmpr_stream.cont && cmpr_stream.cont != stream)
						free(cmpr_stream.cont);
					if (result.cont)
						free(result.cont);

					*uncmpr_addr = NULL;
					*uncmpr_len = 0;
					return -1;
				}

				filter_array += strlen(unfilter_name);
				skip_blank_in_string(&filter_array);
			}

			*uncmpr_len = result.size;
			*uncmpr_addr = (char *)(result.cont);
		}
		else {
			/* error */
			*uncmpr_addr = NULL;
			*uncmpr_len = 0;
			return -1;
		}
	}

	return 0;
}

inline char read_octal(char **line)
{
	int i = 0;
	char ret;
	while (i < 3 && isdigit(**line)) {
		ret *= 8;
		ret += (*((*line)++) - '0');
	}

	return ret;
}

inline int char2hex(char ch)
{
	if (isdigit((int)ch))
		return (ch - '0');
	else if(isupper((int)ch))
		return (10 + ch - 'A');
	else
		return (10 + ch - 'a');
}

int add_char_to_string(char **ret_buf, int *ret_len, char ch)
{
	int index = strlen(*ret_buf);
	if ((index + 1) >= (*ret_len)) {
		*ret_len *= 2;
	}

	*ret_buf = realloc(*ret_buf, *ret_len);
	if (!(*ret_buf)) {
		fprintf(stderr, "error: realloc memory failed\n");
		return -1;
	}

	(*ret_buf)[index++] = ch;
	(*ret_buf)[index] = '\0';

	return 0;
}

int add_wchar_to_unicode_string(wchar_t **ret_buf, int *ret_len, wchar_t ch)
{
	int index = wcslen(*ret_buf);

	if (index + 2 > *ret_len) {
		(*ret_len) *= 2;
	}

	*ret_buf = (wchar_t *)realloc((void *)*ret_buf, (*ret_len) * sizeof(wchar_t));
	if (!(*ret_buf)) {
		fprintf(stderr, "error: realloc memory failed\n");
		return -1;
	}

	(*ret_buf)[index++] = ch;
	(*ret_buf)[index] = (wchar_t)('\0');

	return 0;
}

int append_string(char **buf, int *len, char *str)
{
	int index = strlen(*buf);

	if (index + strlen(str) >= (*len)) {
		while (index + strlen(str) >= (*len)) {
			(*len) *= 2;
		}

		(*buf) = (char *)realloc(*buf, *len);
		if (!(*buf)) {
			fprintf(stderr, "error: realloc memory failed\n");
			return -1;
		}
	}

	strcat(*buf, str);

	return 0;
}

/*
 * return vaule:
 * 0: other
 * 1: font instruction
 * 2: string instruction
 */
int instruction_type(char *line, int len)
{
	char *line_end = NULL;

	line_end = (char *)memchr((void *)line, (int)('\n'), len);

	if (!line_end) {
		line_end = (char *)memchr((void *)line, (int)('\r'), len);
	}

	if (!line_end) {
		line_end = line + len;
	}

	while (line_end > line && isspace((int)(*line_end)))
		--line_end;

	if (line_end == line) 
		return 0;
	else if(!strncasecmp(line_end-1, "TF", 2))
		return 1;
	else if(!strncasecmp(line_end-1, "TJ", 2))
		return 2;
	else 
		return 0;
}

char *parse_str_in_angle_bracket(const char *line, int line_len, struct font *font_ptr)
{
	char *line_end = (char *)line + line_len;

	if (!font_ptr || (!font_ptr->tounicode && !font_ptr->encoding_name)) {
		char *str = NULL;
		int len = 80;
		str = (char *)malloc(len);
		if (!str) {
			fprintf(stderr, "error: malloc memory failed\n");
			return NULL;
		}
		str[0] = '\0';
		char res = '\0';

		while ((line + 1) < line_end) {
			res = char2hex(*(line++)) << 4;
			res |= char2hex(*(line++));
			add_char_to_string(&str, &len, res);
		}
		if ((line + 1) == line_end) {
			res = char2hex(*line) << 4;
			add_char_to_string(&str, &len, res);
		}

		return str;
	}
	else {
		int code = 0;
		int res = 0;
		wchar_t *wstr = NULL;
		int wlen = 80;
		
		wstr = (wchar_t *)malloc(wlen * sizeof(wchar_t));
		if (!wstr) {
			fprintf(stderr, "error: malloc memory failed\n");
			return NULL;
		}
		wstr[0] = (wchar_t)('\0');

		while (line < line_end - 3) {
			code = char2hex(*(line++)) << 4;
			code |= char2hex(*(line++));
			code <<= 8;
			code |= (char2hex(*(line++)) << 4);
			code |= (char2hex(*(line++)));

			if (font_ptr->tounicode) {
				res = get_unicode_value(code, font_ptr->tounicode);
			}
			else {
				res = lookup_word_in_global_cmap(font_ptr->encoding_name, code);
			}

			if (res != -1) {
				add_wchar_to_unicode_string(&wstr, &wlen, (wchar_t)(res));
			}
		}

		setlocale(LC_ALL, "zh_CN.UTF-8");

		char *str = NULL;
		int len = wcslen(wstr) * MB_CUR_MAX + 2;
		str = (char *)malloc(len);
		if (!str) {
			fprintf(stderr, "error: malloc memory failed\n");
			free(wstr);
			return NULL;
		}

		wcstombs(str, wstr, len - 1);
		free(wstr);
		return str;
	}
}

char *parse_str_in_round_brackets(const char *line, int line_len, struct font *font_ptr)
{
#if 0
	/* XXX: debug */
	static int count = 0;
	char *ret_buf = NULL;
	int len = 80;
	
	ret_buf = (char *)malloc(len);
	if (!ret_buf) {
		fprintf(stderr, "error: malloc memory failed\n");
		return NULL;
	}
	ret_buf[0] = '\0';
	
	char *line_end = (char *)line + line_len;
	while (line < line_end) {
		if (*line != '\\') {
			add_char_to_string(&ret_buf, &len, *line);
		}
		line++;
	}
	
	return ret_buf;
#endif

	char *line_end = (char *)line + line_len;

	if (!font_ptr || (!font_ptr->tounicode && !font_ptr->encoding_name)) {
		char *ret_buf = NULL;
		int len = 80;
	
		ret_buf = (char *)malloc(len);
		if (!ret_buf) {
			fprintf(stderr, "error: malloc memory failed\n");
			return NULL;
		}
		ret_buf[0] = '\0';
	
		char *line_end = (char *)line + line_len;
		while (line < line_end) {
			if (*line != '\\') {
				add_char_to_string(&ret_buf, &len, *line);
			}
			line++;
		}
	
		return ret_buf;
	}
	else {
		int code = 0;
		int res = 0;
		wchar_t *wstr = NULL;
		int wlen = 80;
		
		wstr = (wchar_t *)malloc(wlen * sizeof(wchar_t));
		if (!wstr) {
			fprintf(stderr, "error: malloc memory failed\n");
			return NULL;
		}
		wstr[0] = (wchar_t)('\0');

		while (line < line_end) {
			/* XXX: debug */
			//count++;
			/* XXX */
			if (*line == '\\') {
				line++;
				code = (unsigned char )parse_escape_char(&line);
			}
			else { 
				code = (unsigned char )(*(line++));
			}
			code <<= 8;
			if (line >= line_end)
				break;

			if (*line == '\\') {
				line++;
				code |= (unsigned char )parse_escape_char(&line);
			}
			else {
				code |= (unsigned char )(*(line++));
			}

			if (font_ptr->tounicode) {
				res = get_unicode_value(code, font_ptr->tounicode);
			}
			else {
				res = lookup_word_in_global_cmap(font_ptr->encoding_name, code);
			}

			if (res != -1) {
				add_wchar_to_unicode_string(&wstr, &wlen, (wchar_t)(res));
			}
			else {
				/* XXX: debug */
			//	fprintf(stderr, "to unicode failed, code: %x\n", code);
			}
		}

		setlocale(LC_ALL, "zh_CN.UTF-8");

		char *str = NULL;
		int len = wcslen(wstr) * MB_CUR_MAX + 2;
		str = (char *)malloc(len);
		if (!str) {
			fprintf(stderr, "error: malloc memory failed\n");
			free(wstr);
			return NULL;
		}

		wcstombs(str, wstr, len - 1);
		free(wstr);
		return str;
	}
}

int slash_count(char *line, char *seg_end) 
{
	int slash_count = 0;
	while (--seg_end > line) {
		if (*seg_end == '\\') {
			slash_count++;
		}
		else 
			break;
	}

	return slash_count;
}

char *parse_str_in_line(char *line, int line_len, struct font *font_ptr)
{
	char *ret_buf = NULL;
	int len = 80;
	ret_buf = (char *)malloc(len);
	if (!ret_buf) {
		fprintf(stderr, "error: malloc memory failed\n");
		return NULL;
	}
	ret_buf[0] = '\0';

	char *seg_end = NULL;
	char *line_end = NULL;

	line_end = (char *)memchr((void *)line, (int)('\n'), line_len);
	if (!line_end)
		line_end = (char *)memchr((void *)line, (int)('\r'), line_len);
	if (!line_end)
		line_end = line + line_len;

	while (line < line_end) {
		while (*line != '<' && *line != '(' && line < line_end)
			line++;
		if (line == line_end)
			break;

		if (*line == '<') {
			line++;
			seg_end = (char *)memchr(line, (int)('>'), line_end - line);
			if (!seg_end)
				return ret_buf;			
			char *str = parse_str_in_angle_bracket(line, seg_end - line, font_ptr);
			if (str) {
				append_string(&ret_buf, &len, str);
				free(str);
			}
			line = seg_end + 1;
		}
		else if(*line == '(') {
			line++;
			seg_end = (char *)memchr(line, (int)(')'), line_end - line);
			if (!seg_end)
				return ret_buf;

			int count = slash_count(line, seg_end);
			if (count % 2 == 1) {
				while (*(seg_end-1) == '\\' && seg_end+1 < line_end) {
					seg_end++;
					seg_end = (char *)memchr((void *)seg_end, (int)(')'), line_end - seg_end);
					if (!seg_end)
						return ret_buf;

					count = slash_count(line, seg_end);
					if (count % 2 == 0)
						break;
				}
			}

			char *str = parse_str_in_round_brackets(line, seg_end - line, font_ptr);
			//char *str = parse_str_in_angle_bracket(line, seg_end - line, font_ptr);
			if (str) {
				append_string(&ret_buf, &len, str);
				free(str);
			}
			line = seg_end + 1;
		}
	}

	return ret_buf;
}

char *parse_str_in_cont(struct file_dscr *fd, char *cont, int cont_len, struct font *font_list,\
		struct xref_sect *list)
{
	char *ret_buf = NULL;
	int len = 80;

    int ret = 0;

	ret_buf = (char *)malloc(len);
	if (!ret_buf) {
		fprintf(stderr, "error: malloc memory failed\n");
		return NULL;
	}
	ret_buf[0] = '\0';

	char *cont_end = cont + cont_len;
	
	int ins_type = 0;
	struct font *font_ptr = NULL;
	char font_name[128];

	while (cont < cont_end) {
		ins_type = instruction_type(cont, cont_end - cont);
		if (ins_type == 0)
			skip_line(&cont);
		else if (ins_type == 1) { /* font instruction */
			ret = read_name(cont, font_name, 128);
            if (ret >= 0) {
                font_ptr = font_list;
                while (font_ptr) {
                    if (!strcmp(font_name, font_ptr->font_name))
                        break;
                    font_ptr = font_ptr->next;
                }
            }

			skip_line(&cont);
		}
		else if (ins_type == 2) { /* string instruction */
			/* read the string */
			char *str = parse_str_in_line(cont, cont_end - cont, font_ptr);
			if (str) {
				append_string(&ret_buf, &len, str);
				free(str);
			}
			skip_line(&cont);
		}
		else {
			skip_line(&cont);
		}
	}

	return ret_buf;
}

/*
 * read String Object from Page content
 */
char *read_string_in_page_content(struct file_dscr *fd, char *uncmpr_cont, int uncmpr_len,\
		struct font *font_list ,struct xref_sect *list)
{
	char *ret_buf = NULL;
	int len = 1024;
	
	char *begin = uncmpr_cont;
	int embed = 0;
	ret_buf = (char *)malloc(len+1);
	if (!ret_buf) {
		fprintf(stderr, "error: malloc memory failed\n");
		return NULL;
	}

	ret_buf[0] = '\0';
	while (*begin) {
		if (*begin == '(') {
			/* literal string which is enclosed in parentheses */
			embed++;
			while (*(++begin)) {
				if (*begin != '(' && *begin != ')') {
					add_char_to_string(&ret_buf, &len, *begin);
				}
				else if (*begin == '(') {
					if (*(begin-1) != '\\') {
						embed++;
					}
					add_char_to_string(&ret_buf, &len, *begin);
				}
				else {
					/* ch == ')' */
					if (*(begin-1) != '\\')
						embed--;
					if (*(begin-1) == '\\' || embed != 0)
						add_char_to_string(&ret_buf, &len, *begin);
					if (embed == 0)
						break;
				}
			}
		}
		else if (*begin == '<') {
			char res = 0;
			int shift = 4;
			while (*(++begin)) {
				if (*begin == '>') {
					if (res != 0)
						add_char_to_string(&ret_buf, &len, res);
					break;
				}
				else {
					res |= char2hex(*begin) >> shift;
					add_char_to_string(&ret_buf, &len, res);

					if (shift)
						shift = 0;
					else 
						shift = 4;
					res = 0;
				}
			}
		}
		else {
			begin++;
		}
	}

	return ret_buf;
}

char *parse_content(struct file_dscr *fd, obj_disc *cont, struct font *font_list, struct xref_sect *list) 
{
	char *cont_buf = NULL;
	int cont_len = 0;
	int ret = 0;

	ret = decode(cont, &cont_buf, &cont_len, list);

	if (ret == -1) {
		fprintf(stderr, "error: can't decode the content\n");
		return NULL;
	}

	char *str = parse_str_in_cont(fd, cont_buf, cont_len, font_list, list);

	free(cont_buf);

	return str;
}

struct str_node *extract_text_from_page(struct file_dscr *fd, obj_disc *page, struct xref_sect *list)
{
	int ret = 0;

	struct str_node *str_list = NULL, 
					*list_tail = NULL, 
					*list_ptr = NULL;

	char *contArray = NULL;
	char *contArrayEnd = NULL;
	size_t contArraySize = 0;

	contArray = (char *)memmem(page->cont, page->size, (const void *)("/Contents"), 9);
	if (!contArray) {
		fprintf(stdout, "read an empty page\n");
		return NULL;
	}

	struct font *font_list = NULL;
	font_list = get_font_resources(fd, page, list);
	if (!font_list) {
		fprintf(stdout, "can't get font resources in this page\n");
	}

	int offset = 0;
	obj_disc cont;

	int obj_num;
	unsigned short gen;
	char r;

	contArray += 9; /* skip "/Contents" */
	skip_blank_in_string(&contArray);

	if (*contArray == '[') {
		/* the contents is an array */
		/* skip the "[" */
		contArray++;
		size_t size = page->cont + page->size - (void *)contArray;
		contArrayEnd = (char *)memmem((const void *)contArray, size, (const void *)("]"), 1);
		if (!contArrayEnd) {
			fprintf(stderr, "error: can't find the end of an array\n");
			if (font_list)
				free_font_list(font_list);

			return NULL;
		}

		contArraySize = contArrayEnd - contArray;

		skip_blank_in_string(&contArray);
		while (contArray < contArrayEnd) {
			ret = sscanf(contArray, "%d %hd %c", &obj_num, &gen, &r);
			if (ret != 3) {
				fprintf(stderr, "error: can't read reference here\n");
				if (font_list)
					free_font_list(font_list);

				return str_list;
			}
			if (r != 'R') {
				/* seems to be read wrongly */
				if (font_list)
					free_font_list(font_list);

				return str_list;
			}

			/* get the content object */
			offset = get_obj_offset(obj_num, list);
			if (offset == -1) {
				/* error */
				fprintf(stderr, "error: can't get the offset of the content object\n");
				if (font_list)
					free_font_list(font_list);

				return NULL;
			}

			ret = read_object(fd, offset, &cont);
			if (ret == -1) {
				fprintf(stderr, "error: can't read content object\n");
			}
			else {
				/* read the content */				
				char *str = parse_content(fd, &cont, font_list, list);
				/* add the string to list */
				list_ptr = (struct str_node *)malloc(sizeof(struct str_node));
				if (!list_ptr) {
					fprintf(stderr, "error: malloc memory failed\n");
					/* should break and return the read list */
					if (font_list)
						free_font_list(font_list);

					return str_list;
				}
				
				list_ptr->next = NULL;
				list_ptr->str = str;

				if (str_list) {
					list_tail->next = list_ptr;
					list_tail = list_tail->next;
				}
				else {
					str_list = list_tail = list_ptr;
				}
			}

			contArray = memchr(contArray, (unsigned int)('R'), contArraySize);
			if (!contArray) {
				fprintf(stderr, "error: can't find next object reference\n");
				if (font_list)
					free_font_list(font_list);

				return str_list;
			}
			/* skip the "R" */
			contArray++;
			skip_blank_in_string(&contArray);
		}

		if (font_list)
			free_font_list(font_list);

		return str_list;
	}
	else {
		/* only one reference here */
		ret = sscanf(contArray, "%d %hd %c", &obj_num, &gen, &r);
		if (ret != 3) {
			fprintf(stderr, "error: can't read reference here for some error\n");
			if (font_list)
				free_font_list(font_list);

			return NULL;
		}

		/* get the content object */
		offset = get_obj_offset(obj_num, list);
		if (offset == -1) {
			fprintf(stderr, "error: can't get the offset of the content object\n");
			if (font_list)
				free_font_list(font_list);

			return NULL;
		}

		obj_disc cont_obj;

		ret = read_object(fd, offset, &cont_obj);
		if (ret == -1) {
			fprintf(stderr, "error: read an imcomplete object\n");
			if (font_list) 
				free_font_list(font_list);

			return NULL;
		}

		char *stringBuf = parse_content(fd, &cont_obj, font_list, list);

		if (stringBuf) {
			list_ptr = (struct str_node *)malloc(sizeof(struct str_node));
			if (!list_ptr) {
				fprintf(stderr, "error: malloc memory failed\n");
				free(stringBuf);
			}
			else {
				list_ptr->next = NULL;
				list_ptr->str = stringBuf;
			}
		}
		else {
			fprintf(stdout, "can't read any text in the content object\n");
		}
		str_list = list_tail = list_ptr;

		if (font_list)
			free_font_list(font_list);

		return str_list;
	}
}

struct str_node *extract_text_from_pages(struct file_dscr *fd, obj_disc *pages, struct xref_sect *list)
{
	int ret = 0;

	/* check if it's a pages obj */
	void *param = NULL;
	param = memmem(pages->cont, pages->size, (const void *)("/Pages"), 6);
	if (!param) {
		/* this is not a pages obj */
		fprintf(stderr, "error: this is not a pages object\n");
		return NULL;
	}

	param = memmem(pages->cont, pages->size, (const void *)("/Kids"), 5);
	if (!param) {
		/* can't find the kids entry */
		fprintf(stderr, "error: can't find '/Kids' entry in this pages object\n");
		return NULL;
	}

	param += 5;

	char *array = param;
	skip_blank_in_string(&array);

	if (*array != '[') {
		fprintf(stderr, "/Kids entry's value should be an array\n");
		return NULL;
	}

	char *array_end = NULL;
	size_t left = pages->cont + pages->size - (void *)array;
	array_end = array + left;

	int obj_num = 0;
	unsigned short gen = 0;
	char r = '\0';

	int offset = 0;
	obj_disc page;

	struct str_node *str_list = NULL, 
					*list_tail = NULL,
					*list_ptr = NULL;
	
	char flag = 0;

	array++; // skip '['
	skip_blank_in_string(&array);

	while (array < array_end && *array != ']') {
		/* read the content */
		ret = sscanf(array, "%d %hd %c", &obj_num, &gen, &r);
		if (ret != 3) {
			fprintf(stderr, "error: can't read reference here\n");
			return str_list;
		}

		if (r != 'R') {
			/* seems to be read wrongly */
			fprintf(stderr, "error: can't read next reference\n");
			return str_list;
		}

		/* get the content object */
		offset = get_obj_offset(obj_num, list);
		if (offset == -1) {
			/* error */
			fprintf(stderr, "error: can't get the offset of the content object\n");
			return NULL;
		}

		ret = read_object(fd, offset, &page);
		if (ret == -1) {
			fprintf(stderr, "error: can't read content object\n");
		}
		else {
			param = memmem(page.cont, page.size, (const void *)("/Pages"), 6);
			if (param) {
				/* this is still a pages object */
				list_ptr = extract_text_from_pages(fd, &page, list);
				flag = 1;
			}
			else {
				param = memmem((const void *)page.cont, page.size, (const void *)("/Page"), 5);
				if (param) {
					list_ptr = extract_text_from_page(fd, &page, list);
					flag = 1;
				}
				else {
					fprintf(stderr, "error: this is not either a pages or a page object\n");
					flag = 0;
				}
			}

			/* add the cont to list */
			if (flag) {
				if (str_list) {
					list_tail->next = list_ptr;
				}
				else {
					str_list = list_tail = list_ptr;
				}

				while (list_tail && list_tail->next)
					list_tail = list_tail->next;
			}
		}

		array = memchr(array, (int)('R'), array_end - array);
		if (!array) {
			fprintf(stderr, "error: can't find next object reference\n");
			return str_list;
		}
		/* skip the "R" */
		array++;
		skip_blank_in_string(&array);
	}

	return str_list;
}

void free_xref_list(struct xref_sect *list) 
{
	struct xref_sect *ptr = list;
	struct xref_item_body *body1 = NULL,
						  *body2 = NULL;
	while ((ptr = list)) {
		list = list->next;
		body1 = ptr->body;
		ptr->body = NULL;
		while ((body2 = body1)) {
			body1 = body1->next;
			if (body2->array)
				free(body2->array);
			free(body2);
		}
		if (ptr->trailer.id) {
			free(ptr->trailer.id);
		}
		if (ptr->trailer.encrypt) {
			free(ptr->trailer.encrypt);
		}
		if (ptr->trailer.root) {
			free(ptr->trailer.root);
		}
		if (ptr->trailer.info) {
			free(ptr->trailer.info);
		}
		free(ptr);
	}
}

struct str_node *extract_text_from_PDF(struct file_dscr *fd)
{
	int ret = 0;
	struct xref_sect *list = NULL;
	int catalog_offset = 0;
	obj_disc catalog;
	int pages_offset = 0;
	obj_disc pages;

#if 0
	if (fd->size > 5 * 1024 * 1024) {
		fprintf(stdout, "info: pdf file that is bigger than 5M\n");
		return NULL;
	}
#endif
	
	if (fd->size < 1024) {
        fprintf(stderr, "error: pdf file size should be greater than 1024 bytes\n");
        return NULL;
    }

	struct str_node *str_list = NULL;

	ret= read_all_xrefs(fd, &list);
	if (ret == -1) {
		fprintf(stderr, "error: can't read all xref\n");
		return NULL;
	}

	catalog_offset = get_catalog_offset(list);
	if (catalog_offset == -1) {
		fprintf(stderr, "error: can't read catatlog object\n");
		return NULL;
	}
	
	ret = read_object(fd, catalog_offset, &catalog);
	if (ret == -1) {
		fprintf(stderr, "error: can't read catalog for some error\n");
		return NULL;
	}

	pages_offset = get_pages_obj_offset(&catalog, list);
	if (pages_offset == -1) {
		fprintf(stderr, "error: \n");
		return NULL;
	}

	ret = read_object(fd, pages_offset, &pages);
	if (ret == -1) {
		fprintf(stderr, "error: \n");
		return NULL;
	}
	
	str_list = extract_text_from_pages(fd, &pages, list);
	if (!str_list) {
		fprintf(stderr, "error: can't read any text from this file\n");
	}

	free_xref_list(list);

	return str_list;
}

int pdf_extract(char *filename)
{
	struct file_dscr fd;
	char output[1024];
	int ret = 0;

	ret = map_file_to_mem(filename, &fd);
	if (ret == -1) {
		fprintf(stderr, "error: can't map file to memory\n");
		return -1;
	}

	struct str_node *list = extract_text_from_PDF(&fd);

	sprintf(output, "%s.txt", filename);
	dump_to_file(list, output);

	free_str_list(list);

	return 0;
}

int main(int argc, char **argv) 
{
	if (argc != 2) {
		fprintf(stderr, "Usage: pdf2txt pdf_file\n");
		exit(1);
	}

	pdf_global_resource_init();

	pdf_extract(argv[1]);

	pdf_global_resource_release();

	return 0;
}
