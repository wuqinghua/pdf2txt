#ifndef __PDF_H
#define __PDF_H

// #define CATALOG_ENTRY_NUM 28
#define PDF_RESOURCE_PATH "/home/alvin/workspace/projects/pdf2txt/Resources"

enum obj_type { 
	BOOLEAN = 0, 
	NUMERIC, 
	STRING, 
	NAME, 
	ARRAY, 
	DICTIONARY, 
	STREAM, 
	NUL, 
	INDIRECT, 
	TREE
};

enum font_subtype { 
	Type0 = 0, 
	Type1, 
	MMType1, 
	Type3, 
	TrueType, 
	CIDFontType0, 
	CIDFontType2, 
	UNKNOWN_FONTTYPE 
};

typedef struct {
	char *disc;
	enum obj_type type;
} entry;

typedef struct {
	void *cont;
	size_t size;
} obj_disc;

/* 
 * static entry catalogEntries[CATALOG_ENTRY_NUM] = {
 * 	{ "/Type", NAME },
 * 	{ "/Version", NAME },
 * 	{ "/Pages", DICTIONARY },
 * 	{ "/PageLabels", TREE }, // what's a tree type?
 * 	{ "/Names",	DICTIONARY },
 * 	{ "/Dests", DICTIONARY },
 * 	{ "/ViewerPreferences", DICTIONARY },
 * 	{ "/PageLayout", NAME }, 
 * 	{ "/PageMode", NAME },
 * 	{ "/Outlines", DICTIONARY },
 * 	{ "/Thread", ARRAY },
 * 	{ "/OpenAction", ARRAY }, // or dictionary
 * 	{ "/AA", DICTIONARY },
 * 	{ "/URI", DICTIONARY },
 * 	{ "/AcroForm", DICTIONARY }, 
 * 	{ "/Metadata", STREAM },
 * 	{ "/StructTreeRoot", DICTIONARY },
 * 	{ "/MarkInfo", DICTIONARY },
 * 	{ "/Lang", STRING },
 * 	{ "/SpiderInfo", DICTIONARY },
 * 	{ "/OutputIntents", ARRAY },
 * 	{ "/PieceInfo", DICTIONARY },
 * 	{ "/OCProperties", DICTIONARY },
 * 	{ "/Perms", DICTIONARY },
 * 	{ "/Legal", DICTIONARY },
 * 	{ "/Requirements", ARRAY },
 * 	{ "/Collection", DICTIONARY },
 * 	{ "/NeedsRendering", BOOLEAN }
 * };
 */

struct ref_node {
	int obj_num;
	uint16_t gen;
	char keyword;
	struct ref_node *next;
};

struct indirect_obj {
	int obj_num;
	uint16_t gen;
	char keyword;
};

struct xref_item {
	int offset;
	uint16_t gen;
	char status;
    char unused;
};

struct xref_item_body{
	int start;
	int count;
	struct xref_item *array;
	struct xref_item_body *next;
};

struct trailer {
	int size;
	int prev;
	int xref_stm;
	char *id;
	struct indirect_obj *encrypt;
	struct indirect_obj *root;
	struct indirect_obj *info;
};

struct xref_sect {
	struct xref_item_body *body;
	struct trailer trailer;
	struct xref_sect *next;
};

struct char_map {
	uint16_t from;
	uint16_t to;
};

struct char_map_node {
	struct char_map *char_map_array;
	int char_map_count;
	struct char_map_node *next;
};

struct range_map {
	uint16_t begin;
	uint16_t end;
	uint16_t to;
};

struct range_map_node {
	struct range_map *range_map_array;
	int range_map_count;
	struct range_map_node *next;
};

struct tounicode_map {
	int code_range_begin;
	int code_range_end;
	struct char_map_node *char_map_list;
	struct range_map_node *range_map_list;
};

struct cmap {
	char cmap_file_name[64];
	uint16_t array[65536];
	struct cmap *next;
};

struct font{
	char font_name[20];
	enum font_subtype subtype;
	struct tounicode_map *tounicode;
	char *encoding_name;
	struct font *next;
};

int pdf_global_resource_init();
int pdf_global_resource_release();
struct str_node *extract_text_from_PDF(struct file_dscr *fd);

static int read_cmap_file(const char *path, const char *cmap_file, struct cmap *cmap_ptr);
static int read_range_map_in_cmap_file(const char *stream, int count, uint16_t *array, size_t size);
static int convert_to_cmap_file_name(const char *from, char *to);
static int lookup_word_in_global_cmap(const char *font_name, uint16_t code);
 
static inline int is_name_obj(char *content);
static inline int is_reference(char *content);
static inline void skip_line(char **bufp);
static inline void skip_blank_in_string(char **bufp);
static inline void skip_words(char **bufp, int num);

static inline int char2hex(char ch);
static int append_string(char **buf, int *len, char *str);
static int add_char_to_string(char **ret_buf, int *ret_len, char ch);
static int add_wchar_to_unicode_string(wchar_t **ret_buf, int *ret_len, wchar_t ch);
static int read_name(char *file_buf, char *buf, const int name_len_max);
static int read_object(struct file_dscr *fd, int obj_begin, obj_disc *obj);
static int get_catalog_offset(struct xref_sect *list);
static int get_obj_offset(int obj_num, struct xref_sect *list);
static int get_array_obj_len(int *len, const char *file_buf);
static int read_xref(struct file_dscr *fd, off_t fileOffset, struct xref_sect **listAddr);
static int read_all_xrefs(struct file_dscr *fd, struct xref_sect **listAddr);
static int get_pages_obj_offset(obj_disc *catalog, struct xref_sect *list);
static void free_xref_list(struct xref_sect *list);

static int parse_cmap_file(struct file_dscr *fd, uint16_t *array, size_t size);
static int free_font_list(struct font *font_list);
static struct font *get_font_list(struct file_dscr *fd, char *font_buf, size_t size, struct xref_sect *list);
static enum font_subtype get_font_subtype(char *subtype_name);
static int get_unicode_value(int code, struct tounicode_map *map);
static int parse_tounicode_map(const char *tounicode_stream, struct tounicode_map *map);
static int read_char_map(char *stream, int count, struct char_map_node *node);
static int read_range_map(char *stream, int count, struct range_map_node *node);
static int parse_tounicode_map(const char *tounicode_stream, struct tounicode_map *map);
static struct font *parse_font(struct file_dscr *fd, char *font_name, obj_disc *font_obj, struct xref_sect *list);
static struct font *get_font_resources(struct file_dscr *fd, obj_disc *page, struct xref_sect *list);

static int decode(obj_disc *content, char **uncmpr_addr, int *uncmpr_len, struct xref_sect *list);
static int flate_decode(obj_disc *content, obj_disc *result);
static int unfilter(obj_disc *content, obj_disc *result, const char *filter);
static int instruction_type(char *line, int len);
static int decode(obj_disc *content, char **uncmpr_addr, int *uncmpr_len, struct xref_sect *list);
static void free_xref_list(struct xref_sect *list);

static struct str_node *extract_text_from_page(struct file_dscr *fd, obj_disc *page, struct xref_sect *list);
static struct str_node *extract_text_from_pages(struct file_dscr *fd, obj_disc *pages, struct xref_sect *list);

static char *parse_content(struct file_dscr *fd, obj_disc *cont, struct font *font_list, struct xref_sect *list);
static char *parse_str_in_cont(struct file_dscr *fd, char *cont, int cont_len, struct font *font_list, struct xref_sect *list);
static char *parse_str_in_line(char *line, int len, struct font *font_ptr);
static char *parse_str_in_round_brackets(const char *line, int line_len, struct font *font_ptr);
static char *parse_str_in_angle_bracket(const char *line, int line_len, struct font *font_ptr);

char *read_string_in_page_content(struct file_dscr *fd, char *uncmpr_cont, int uncmpr_len, struct font *font_list, struct xref_sect *list);

#endif
