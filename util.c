#include <assert.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <iconv.h>
#include <locale.h>
#include <errno.h>
#include <string.h>
#include <pthread.h>
#include <ctype.h>

#include "util.h"

int seek_read(FILE *p, uint32_t offset, char *ptr, uint32_t size)
{
	int ret;
	ret = fseek(p, offset, SEEK_SET);
	if (ret == -1) {
		printf("error: seek error in file\n");
		return -1;
	}
	ret = fread(ptr, 1, size, p);
	if (ret != size) {
		printf("error: read error in file\n");
		return -1;
	}
	return ret;
}

int read_in_mem(struct file_dscr *fd, uint32_t offset, void *ptr, uint32_t size) 
{
	if ((offset + size) > fd->size) {
		// fprintf(stderr, "read beyond the file's boundary\n");
		return -1;
	}

	memcpy(ptr, (fd->cont + offset), size);

	return 0;
}

/* write the text in stringList to file */
int dump_to_file(struct str_node *list, const char *outputfile)
{
	FILE *fp = fopen(outputfile, "w");
	if (!fp) {
		fprintf(stderr, "can't open file: %s\n", outputfile);
		return -1;
	}

	while (list) {
		if (list->str) {
			fprintf(fp, "%s\n", list->str);
		}

		list = list->next;
	}
	fclose(fp);

	return 0;
}

void free_str_list(struct str_node *list) {
	struct str_node *ptr;
	while ((ptr = list) != NULL) {
		list = list->next;
		if (ptr->str)
			free(ptr->str);
		free(ptr);
	}
}

int map_file_to_mem(char *filename, struct file_dscr *fd)
{
	int file = open(filename, O_RDONLY);
	if (file == -1) {
		fprintf(stderr, "file open error\n");
		return -1;
	}

	struct stat statbf;
	fstat(file, &statbf);
	fd->size = statbf.st_size;

	fd->cont = mmap(NULL, fd->size, PROT_READ, MAP_SHARED, file, 0);
	
	if (!fd->cont) {
		fprintf(stderr, "Can't Map File to Memory\n");
		return -1;
	}

	close(file);
	
	return 0;
}
